import 'dart:ui';

abstract class AppConfig {
  static String _applicationName = 'HeeseChat';
  static String get applicationName => _applicationName;
  static String _applicationWelcomeMessage;
  static String get applicationWelcomeMessage => _applicationWelcomeMessage;
  static String _defaultHomeserver = 'matrix.gymnasiumsteglitz.de';
  static String get defaultHomeserver => _defaultHomeserver;
  static String jitsiInstance = 'https://meet.jit.si/';
  static double fontSizeFactor = 1.0;
  static const bool allowOtherHomeservers = false;
  static const bool enableRegistration = false;
  static const Color primaryColor = Color(0xFF217dc0);
  static const Color primaryColorLight = Color(0xFFb9dbf3);
  static const Color secondaryColor = Color(0xFFb9dbf3);
  static String _privacyUrl =
      'https://gymnasiumsteglitz.de/datenschutzerklaerung';
  static String get privacyUrl => _privacyUrl;
  static const String appId = 'org.nhcham.heesechat.HeeseChat';
  static const String appOpenUrlScheme = 'org.nhcham.heesechat';
  static String _webBaseUrl = 'https://fluffychat.im/web';
  static String get webBaseUrl => _webBaseUrl;
  static const String sourceCodeUrl =
      'https://gitlab.com/micha.specht/heese-chat';
  static const String supportUrl =
      'https://gitlab.com/famedly/fluffychat/issues';
  static const bool enableSentry = false;
  static const String sentryDns = '';
  static bool renderHtml = true;
  static bool hideRedactedEvents = true;
  static bool hideUnknownEvents = true;
  static const bool hideTypingUsernames = false;
  static const bool hideAllStateEvents = false;
  static const String inviteLinkPrefix = 'https://matrix.to/#/';
  static const String schemePrefix = 'matrix:';
  static const String pushNotificationsChannelId = 'heesechat_push';
  static const String pushNotificationsChannelName = 'HeeseChat push channel';
  static const String pushNotificationsChannelDescription =
      'Push notifications for HeeseChat';
  static const String pushNotificationsAppId = 'org.nhcham.heesechat';
  static const String pushNotificationsGatewayUrl =
      'https://push.matrix.gymnasiumsteglitz.de/_matrix/push/v1/notify';
  static const String pushNotificationsPusherFormat = 'event_id_only';
  static const String emojiFontName = 'Noto Emoji';
  static const String emojiFontUrl =
      'https://github.com/googlefonts/noto-emoji/';
  static const double borderRadius = 12.0;
  static const double columnWidth = 360.0;
  static bool isTeacher = false;

  static void loadFromJson(Map<String, dynamic> json) {
    if (json['application_name'] is String) {
      _applicationName = json['application_name'];
    }
    if (json['application_welcome_message'] is String) {
      _applicationWelcomeMessage = json['application_welcome_message'];
    }
    if (json['default_homeserver'] is String) {
      _defaultHomeserver = json['default_homeserver'];
    }
    if (json['jitsi_instance'] is String) {
      jitsiInstance = json['jitsi_instance'];
    }
    if (json['privacy_url'] is String) {
      _webBaseUrl = json['privacy_url'];
    }
    if (json['web_base_url'] is String) {
      _privacyUrl = json['web_base_url'];
    }
    if (json['render_html'] is bool) {
      renderHtml = json['render_html'];
    }
    if (json['hide_redacted_events'] is bool) {
      hideRedactedEvents = json['hide_redacted_events'];
    }
    if (json['hide_unknown_events'] is bool) {
      hideUnknownEvents = json['hide_unknown_events'];
    }
  }
}
