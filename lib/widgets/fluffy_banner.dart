import 'package:flutter/material.dart';

class FluffyBanner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image.asset('assets/banner.png');
  }
}
