import 'package:cached_network_image/cached_network_image.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:vrouter/vrouter.dart';

import '../homeserver_picker.dart';
import 'package:fluffychat/widgets/default_app_bar_search_field.dart';
import 'package:fluffychat/widgets/fluffy_banner.dart';
import 'package:fluffychat/config/app_config.dart';
import 'package:fluffychat/widgets/layouts/one_page_card.dart';
import 'package:fluffychat/utils/platform_infos.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:pinput/pin_put/pin_put_state.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../utils/localized_exception_extension.dart';

import 'package:matrix/matrix.dart';
import 'dart:developer' as developer;

class HomeserverPickerView extends StatelessWidget {
  final HomeserverPickerController controller;

  const HomeserverPickerView(this.controller, {Key key}) : super(key: key);

  // void releaseFocus(context) {
  //   developer.log('releasing focus');
  //   FocusScope.of(context).requestFocus(new FocusNode());
  // }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = (controller.loginTag == null)
        ? AppBar(
            elevation: 0,
            title: Text('HeeseChat'),
          )
        : AppBar(
            elevation: 0,
            title: Text('HeeseChat'),
            leading: IconButton(
                onPressed: () => {controller.resetLogin()},
                icon: Icon(Icons.arrow_back)));

    return OnePageCard(
      child: Scaffold(
        appBar: appBar,
        body: ListView(children: [
          Hero(
            tag: 'loginBanner',
            child: FluffyBanner(),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(12, 16, 12, 12),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                if (controller.loginTag == null)
                  Text(L10n.of(context).loginHint),
                if (controller.loginTag == null)
                  TextField(
                    autocorrect: false,
                    autofocus: false,
                    keyboardType: TextInputType.emailAddress,
                    controller: controller.usernameController,
                    readOnly: controller.loginTag != null,
                    onSubmitted: (s) => controller.request_login_code(context),
                    // onEditingComplete: () => releaseFocus(context),
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.account_box_outlined),
                        labelText: L10n.of(context).pleaseEnterYourEmail),
                  ),
                if (controller.loginTag == null)
                  Container(
                    height: 60,
                    child: OutlinedButton.icon(
                      onPressed: controller.busy
                          ? null
                          : () => controller.request_login_code(context),
                      icon: Icon(Icons.login_outlined),
                      label: Text(L10n.of(context).requestLoginCode),
                    ),
                  ),
                if (controller.loginTag != null)
                  Text(L10n.of(context).findPinInEmail),
                if (controller.loginTag != null &&
                    controller.clipboardPin != null)
                  OutlinedButton(
                      onPressed: () {
                        controller.passwordController.text =
                            controller.clipboardPin;
                      },
                      child: Text(L10n.of(context)
                          .pastePinFromClipboard(controller.clipboardPin))),
                if (controller.loginTag != null && PlatformInfos.isMobile)
                  PinPut(
                    autofocus: true,
                    fieldsCount: 6,
                    controller: controller.passwordController,
                    textStyle: TextStyle(fontSize: 24),
                    keyboardType: TextInputType.number,
                    focusNode: controller.pinPutFocusNode,
                    pinAnimationType: PinAnimationType.scale,
                    submittedFieldDecoration: BoxDecoration(
                      color: MediaQuery.of(context).platformBrightness ==
                              Brightness.dark
                          ? Color(0x10ffffff)
                          : Color(0x10000000),
                      border: Border.all(
                          color: MediaQuery.of(context).platformBrightness ==
                                  Brightness.dark
                              ? Color(0x40ffffff)
                              : Color(0x40000000)),
                      borderRadius: BorderRadius.circular(150.0),
                    ),
                    selectedFieldDecoration: BoxDecoration(
                      color: MediaQuery.of(context).platformBrightness ==
                              Brightness.dark
                          ? Color(0x20ffffff)
                          : Color(0x20000000),
                      border: Border.all(
                          color: MediaQuery.of(context).platformBrightness ==
                                  Brightness.dark
                              ? Color(0x40ffffff)
                              : Color(0x40000000)),
                      borderRadius: BorderRadius.circular(150.0),
                    ),
                    followingFieldDecoration: BoxDecoration(
                        // border: Border.all(color: Color(0x80ff0000)),
                        borderRadius: BorderRadius.circular(150.0),
                        // border: Border.all(color: Color(0x40000000)),
                        color: MediaQuery.of(context).platformBrightness ==
                                Brightness.dark
                            ? Color(0x10ffffff)
                            : Color(0x10000000)),
                    onClipboardFound: controller.clipboard_updated_with_string,
                    eachFieldPadding: EdgeInsets.all(0.0),
                    eachFieldMargin: EdgeInsets.all(4.0),
                    fieldsAlignment: MainAxisAlignment.spaceAround,
                  ),
                if (controller.loginTag != null && !PlatformInfos.isMobile)
                  TextField(
                      autofocus: true,
                      controller: controller.passwordController,
                      keyboardType: TextInputType.number,
                      onSubmitted: controller.try_submit_s,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.lock_outlined),
                      )),
                if (controller.loginTag != null && !PlatformInfos.isMobile)
                  ElevatedButton(
                    onPressed: controller.busy
                        ? null
                        : () => controller.try_submit_s(''),
                    child: Text(L10n.of(context).login),
                  ),
                if (controller.busy) LinearProgressIndicator(),
                if (controller.error != null)
                  Text(
                    controller.error,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.red[800]),
                  ),
              ]
                  .map((widget) => Container(
                      child: widget, padding: EdgeInsets.only(bottom: 16)))
                  .toList(),
            ),
          ),
        ]),
        // bottomNavigationBar: Material(
        //   elevation: 7,
        //   color: Theme.of(context).scaffoldBackgroundColor,
        //   child: Wrap(
        //     alignment: WrapAlignment.center,
        //     children: [
        //       TextButton(
        //         onPressed: () => launch(AppConfig.privacyUrl),
        //         child: Text(
        //           L10n.of(context).privacy,
        //           style: TextStyle(
        //             decoration: TextDecoration.underline,
        //             color: Colors.blueGrey,
        //           ),
        //         ),
        //       ),
        //       TextButton(
        //         onPressed: () => PlatformInfos.showDialog(context),
        //         child: Text(
        //           L10n.of(context).about,
        //           style: TextStyle(
        //             decoration: TextDecoration.underline,
        //             color: Colors.blueGrey,
        //           ),
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
      ),
    );
  }
}
