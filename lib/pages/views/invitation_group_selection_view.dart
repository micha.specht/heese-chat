import 'package:fluffychat/pages/invitation_group_selection.dart';
import 'package:fluffychat/widgets/default_app_bar_search_field.dart';

import 'package:matrix/matrix.dart';
import 'package:fluffychat/widgets/avatar.dart';
import 'package:fluffychat/widgets/layouts/max_width_body.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:vrouter/vrouter.dart';

class InvitationGroupSelectionView extends StatelessWidget {
  final InvitationGroupSelectionController controller;

  const InvitationGroupSelectionView(this.controller, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final room = Matrix.of(context).client.getRoomById(controller.roomId);
    final groupName =
        room.name?.isEmpty ?? false ? L10n.of(context).group : room.name;
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.close_outlined),
            onPressed: () =>
                VRouter.of(context).push('/rooms/${controller.roomId}'),
          ),
          titleSpacing: 0,
          title: Text(L10n.of(context).inviteGroup)),
      body: MaxWidthBody(
        withScrolling: true,
        child: FutureBuilder<List<dynamic>>(
          future: controller.getGroups(context),
          builder: (BuildContext context, snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            final groups = snapshot.data[0];
            final group_size = snapshot.data[1];
            return ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: groups.length,
              itemBuilder: (BuildContext context, int i) => ListTile(
                title: Text(groups[i]),
                subtitle: Text(L10n.of(context).nUsers(group_size[i])),
                onTap: () => controller.inviteGroupAction(
                    context, groups[i], group_size[i]),
              ),
            );
          },
        ),
      ),
    );
  }
}
