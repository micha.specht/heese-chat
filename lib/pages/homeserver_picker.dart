import 'dart:async';
import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/services.dart';
import 'package:matrix/matrix.dart';
import 'package:fluffychat/pages/views/homeserver_picker_view.dart';
import 'package:fluffychat/utils/famedlysdk_store.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:fluffychat/config/app_config.dart';
import 'package:fluffychat/config/setting_keys.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import '../utils/localized_exception_extension.dart';
import 'package:vrouter/vrouter.dart';
import 'package:fluffychat/utils/platform_infos.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:uni_links/uni_links.dart';
import 'package:universal_html/html.dart' as html;

import '../main.dart';

class HomeserverPicker extends StatefulWidget {
  @override
  HomeserverPickerController createState() => HomeserverPickerController();
}

class HomeserverPickerController extends State<HomeserverPicker> {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final FocusNode pinPutFocusNode = FocusNode();
  // final clipboardNotifier = ClipboardStatusNotifier();

  bool busy = false;
  String matrix_id = null;
  String error = null;
  String loginTag = null;
  String domain = AppConfig.defaultHomeserver;
  String clipboardPin = null;

  @override
  void initState() {
    super.initState();
    if (PlatformInfos.isMobile) {
      passwordController.addListener(pin_updated);
      // if (PlatformInfos.isAndroid) {
      // clipboardNotifier.addListener(clipboard_updated);
      // }
    }
  }

  @override
  void dispose() {
    if (PlatformInfos.isMobile) {
      passwordController.removeListener(pin_updated);
      passwordController.dispose();
      // if (PlatformInfos.isAndroid) {
      // clipboardNotifier.removeListener(clipboard_updated);
      // }
    }
    super.dispose();
  }

  void try_submit_s(String s) {
    login();
  }

  void pin_updated() {
    String original_pin = passwordController.text;
    String pin = original_pin.replaceAll(RegExp(r"\D"), '');
    if (original_pin != pin) {
      passwordController.text = pin;
    }
    if (pin.length == 6) {
      login();
    }
  }

  void clipboard_updated() async {
    developer.log("Weeeee!");
    ClipboardData data = await Clipboard.getData(Clipboard.kTextPlain);
    String pin = data.text;
    await clipboard_updated_with_string(pin);
  }

  void clipboard_updated_with_string(String pin) async {
    if (pin.length == 6 && RegExp(r'^\d{6}$').hasMatch(pin)) {
      setState(() => clipboardPin = pin);
    } else {
      setState(() => clipboardPin = null);
      pinPutFocusNode.requestFocus();
    }
  }

  void request_login_code([_]) async {
    final matrix = Matrix.of(context);
    developer.log(context.toString());
    developer.log(matrix.toString());
    if (usernameController.text.isEmpty) {
      setState(
          () => error = L10n.of(context).pleaseEnterYourSchoolEmailAddress);
    } else {
      setState(() => error = null);
    }
    final String email = usernameController.text;
    if (email.isEmpty || !email.contains('.') || !email.contains('.')) {
      setState(
          () => error = L10n.of(context).pleaseEnterYourSchoolEmailAddress);
      return;
    }

    setState(() => busy = true);

    try {
      final response = await http.post(
          Uri.parse('https://dashboard.gymnasiumsteglitz.de/api/login'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8'
          },
          body:
              jsonEncode(<String, String>{'email': email, 'purpose': 'chat'}));
      try {
        final data = jsonDecode(response.body);
        String error_message = data['error'];
        if (error_message.isNotEmpty) {
          if (error_message == 'no_invitation_found') {
            error_message = L10n.of(context).noInvitationFound;
          }
          setState(() => error = error_message);
          return setState(() => busy = false);
        }
      } catch (exception) {}
      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        setState(() => loginTag = data['tag']);
        setState(() => matrix_id = data['chat_handle']);
      } else {
        setState(() => error = 'Fehler bei der Anmeldung.');
        return setState(() => busy = false);
      }
      return setState(() => busy = false);
    } on MatrixException catch (exception) {
      String error_message = exception.errorMessage;
      setState(() => error = error_message);
      return setState(() => busy = false);
    } catch (exception) {
      setState(() => error = exception.toString());
      return setState(() => busy = false);
    }
  }

  void login([_]) async {
    final matrix = Matrix.of(context);
    final pin = passwordController.text;
    if (pin.isEmpty) {
      setState(() => error = L10n.of(context).pleaseEnterYourLoginCode);
    } else {
      setState(() => error = null);
    }

    setState(() => busy = true);
    try {
      await matrix.client
          .checkHomeserver("https://${AppConfig.defaultHomeserver}");
      final username = usernameController.text;
      AuthenticationIdentifier identifier = AuthenticationUserIdentifier(
          user: '@${matrix_id}:gymnasiumsteglitz.de');
      await matrix.client.login(
          identifier: identifier,
          // To stay compatible with older server versions
          // ignore: deprecated_member_use
          user: identifier.type == AuthenticationIdentifierTypes.userId
              ? username
              : null,
          password: '${loginTag}/${pin}',
          initialDeviceDisplayName: PlatformInfos.clientName);
      AppConfig.isTeacher = false;
      final request_json = jsonEncode(<String, String>{
        'matrix_id': matrix.client.userID,
        'access_token': matrix.client.accessToken,
        'code': "${loginTag}/${pin}"
      });
      developer.log(request_json);
      final response = await http.post(
          Uri.parse(
              'https://dashboard.gymnasiumsteglitz.de/api/store_matrix_access_token'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8'
          },
          body: request_json);
      if (response.statusCode == 200) {
        try {
          developer.log(response.body);
          final data = jsonDecode(response.body);
          // todo: store whether we're a teacher or not
          AppConfig.isTeacher = data['teacher'];
        } catch (exception) {}
      }
      developer.log("LOGIN succeeded");
      setState(() => VRouter.of(context).pushReplacement('/'));
    } on MatrixException catch (exception) {
      developer.log(exception.toString());
      pinPutFocusNode.requestFocus();
      if (this.mounted) {
        setState(() => error = L10n.of(context).wrongLoginCode);
        setState(() => passwordController.text = '');
        setState(() => busy = false);
      }
    } catch (exception) {
      developer.log(exception.toString());
      if (this.mounted) {
        setState(() => error = exception.toString());
        setState(() => passwordController.text = '');
        setState(() => busy = false);
      }
    }
  }

  void resetLogin() {
    setState(() => loginTag = null);
    setState(() => error = null);
    setState(() => busy = false);
    setState(() => matrix_id = null);
    setState(() => passwordController.text = '');
  }

  bool _initialized = false;

  @override
  Widget build(BuildContext context) {
    Matrix.of(context).navigatorContext = context;
    if (!_initialized) {
      _initialized = true;
    }
    return HomeserverPickerView(this);
  }
}

class IdentityProvider {
  final String id;
  final String name;
  final String icon;
  final String brand;

  IdentityProvider({this.id, this.name, this.icon, this.brand});

  factory IdentityProvider.fromJson(Map<String, dynamic> json) =>
      IdentityProvider(
        id: json['id'],
        name: json['name'],
        icon: json['icon'],
        brand: json['brand'],
      );
}
