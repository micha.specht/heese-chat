import 'dart:async';
import 'dart:convert';
import 'dart:developer' as developer;

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:matrix/matrix.dart';
import 'package:fluffychat/pages/views/invitation_group_selection_view.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:vrouter/vrouter.dart';

import '../utils/localized_exception_extension.dart';

class InvitationGroupSelection extends StatefulWidget {
  const InvitationGroupSelection({Key key}) : super(key: key);

  @override
  InvitationGroupSelectionController createState() =>
      InvitationGroupSelectionController();
}

class InvitationGroupSelectionController
    extends State<InvitationGroupSelection> {
  TextEditingController controller = TextEditingController();
  String currentSearchTerm;
  bool loading = false;
  List<Profile> foundProfiles = [];
  Timer coolDown;
  List<String> groups = <String>[];
  Map<String, dynamic> ids_for_group = Map();
  List<int> group_size = <int>[];

  String get roomId => VRouter.of(context).pathParameters['roomid'];

  Future<List<dynamic>> getGroups(BuildContext context) async {
    final client = Matrix.of(context).client;
    final room = client.getRoomById(roomId);
    final participants = await room.requestParticipants();
    groups = <String>[];
    ids_for_group = Map();
    group_size = <int>[];

    participants.removeWhere(
      (u) => ![Membership.join, Membership.invite].contains(u.membership),
    );
    final response = await http.post(
        Uri.parse(
            'https://dashboard.gymnasiumsteglitz.de/api/fetch_matrix_groups'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        },
        body: jsonEncode(<String, String>{
          'access_token': client.accessToken,
        }));
    if (response.statusCode == 200) {
      // try {
      final data = jsonDecode(response.body);
      data['group_order'].forEach((title) {
        groups.add(title);
        group_size.add(data['groups'][title].length);
        ids_for_group[title] = data['groups'][title];
      });
      // } catch (exception) {}
    }

    return [groups, group_size];
  }

  void inviteGroupAction(BuildContext context, String title, int n) async {
    if (OkCancelResult.ok !=
        await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context).inviteGroupNUsers(title, n),
          okLabel: L10n.of(context).ok,
          cancelLabel: L10n.of(context).cancel,
        )) {
      return;
    }

    // // show the dialog
    // showDialog(
    //   context: context,
    //   builder: (BuildContext context) {
    //     return AlertDialog(
    //       title: Text("My title"),
    //       content: Text("This is my message."),
    //       actions: [
    //         TextButton(
    //           child: Text("OK"),
    //           onPressed: () {
    //             Navigator.of(context).pop();
    //           },
    //         ),
    //       ],
    //     );
    //   },
    // );

    VRouter.of(context).pop();
    final room = Matrix.of(context).client.getRoomById(roomId);
    for (int i = 0; i < ids_for_group[title].length; i++) {
      String user_id = ids_for_group[title][i];
      try {
        await room.invite(user_id);
        await Future.delayed(Duration(milliseconds: 200));
      } catch (e) {
        developer.log("!!! Exception inviting $user_id: ${e.toString()}");
      }
    }
  }

  @override
  Widget build(BuildContext context) => InvitationGroupSelectionView(this);
}
